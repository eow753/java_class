package com.yedam.API;

public class BoxingExample {
	public static void main(String[] args) {
		// Boxing 기본타입 -> 객체로 포장
		// Boxing
		Integer obj1 = new Integer(100);
		Integer obj2 = new Integer("200");
		Integer obj3 = new Integer("300");
		Integer obj4 = Integer.valueOf(100);
		Integer obj5 = Integer.valueOf("400");

		// unBoxing
		int value1 = obj1.intValue();
		int value2 = obj2.intValue();
		int value3 = obj3.intValue();
		int value4 = obj4.intValue();
		int value5 = obj5.intValue();

		System.out.println(value1);
		System.out.println(value2);
		System.out.println(value3);
		System.out.println(value4);
		System.out.println(value5);

		// 자동 박싱
		Integer obj6 = 100;
		System.out.println("value : " + obj6.intValue());

		// 대입 시 자동 언박싱
		int value6 = obj6;

		// 연산 시 자동 언박싱
		int value7 = obj6 + 300;
		System.out.println("value : " + value7);

		// 포장 값 비교
		Integer obj7 = 300;
		Integer obj8 = 300;

		System.out.println(obj7 == obj8);
		System.out.println(obj7.intValue() == obj8.intValue());

		Boolean obj9 = true;
		Boolean obj10 = true;

		System.out.println("Boolean 비교 : " + (obj9 == obj10));
		
		Byte obj11 = -100;
		Byte obj12 = -100;
		System.out.println("Byte 비교 : " + (obj11 == obj12));
	}
}
