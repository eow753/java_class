package com.yedam.API;

public class ObjectExample {

	public static void main(String[] args) {
		Object obj1 = null;
		Object obj2 = new Object();

		System.out.println(obj1);
		System.out.println(obj2);

//		System.out.println(obj1.toString());
		System.out.println(obj2.toString());

		Member member = new Member("123");
		member.name = "김또치";
		member.ssn = "698765-1236547";
		System.out.println(member.toString());

		// system 클래스
		// 강제종료
//		for (int i = 0; i <= 10; i++) {
//			System.out.println(i);
//			if (i == 5) {
//				System.exit(0);
//			}
//		}
//		System.out.println("프로그램 정상 종료");

		// 현재시각읽기
		long time1 = System.nanoTime();
		int sum = 0;
		for(int i = 0; i<=1000000; i++) {
			sum +=i;
		long time2 = System.nanoTime();
		System.out.println(time1);
		System.out.println(time2);
		System.out.println("1~100000까지 의 합 : " + sum);
		System.out.println("소요시간 : " + (time2 - time1)+"나노 초 소요") ;
		
		System.out.println("Class 클래스");
		
		Class clazz = Member.class;
		System.out.println("첫 번째 방법" + clazz);
		
	clazz = Class.forName("com.yedam.API.Member");
	System.out.println(clazz);

		System.out.println(clazz.getName());
		System.out.println(clazz.getSimpleName());
		System.out.println(clazz.getPackageName());
		System.out.println(clazz.getPackage().getName());
		
		//파일 경로 읽어 오기 - 절.대.경.로
		String photoPath = clazz.getResource("dog.jpg").getPath();
		System.out.println(photoPath);
	}
}
