package com.yedam.condition;

import java.util.Scanner;

public class Exam02 {
	public static void main(String[] args) {
		// 학점 계산하기
		// switch문으로 해결
		// 사용자가 입력한 점수를 토대로 학점을 출력
		// 90점 이상은 A
		// 90~80 B
		// 80~70 C
		// 70~60 D
		Scanner sc = new Scanner(System.in);
		System.out.println("성적 입력 > ");
		int score = Integer.parseInt(sc.nextLine());
		int num = score / 10;
		
		// switch문은 부등호 못 씀
		switch(num) {
		case 10:
		case 9:
			System.out.println("A");
			break;
		case 8:
			System.out.println("B");
			break;
		case 7:
			System.out.println("C");
			break;
		case 6:
			System.out.println("D");
			break;
		default:
			break;
		}
	}
}
