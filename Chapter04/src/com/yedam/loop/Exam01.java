package com.yedam.loop;

public class Exam01 {
	public static void main(String[] args) {
		int sum = 0;
		sum = sum + 1;
		sum = sum + 2;
		sum = sum + 3;
		sum = sum + 4;
		sum = sum + 5;

		// 1~5까지의 합을 구하는 반복문
		for (int i = 0; i <= 5; i++) {
			sum = sum + 1;
		}

		// 짝수 구하는 반복문
		// 1) 규칙
		// 2, 4, 6, 8, 10, 12, 14...
		// i % 2 == 0;
		for (int i = 1; i <= 100; i++) {
			if (i % 2 == 0) {
				System.out.println(i);
			}
		}

		// 1~100사이에서 홀수 구하는 반복문
		for (int i = 1; i <= 100; i++) {
			if (i % 2 != 0) {
				System.out.println(i);
			}
		}

		// 1~100사이에서 2의 배수 또는 3의 배수 찾기
		for (int i = 1; i <= 100; i++) {
			if (i % 2 == 0 || i % 3 == 0) {
				System.out.println(i + "는 2의 배수 또는 3의 배수입니다.");
			}
		}

		// 구구단 출력
		// 만약, 2단 출력
		for (int i = 1; i < 10; i++) {
			System.out.println("2 x " + i + " = " + (2 * i));
		}

		for (int i = 2; i < 10; i++) {
			for (int j = 1; j < 10; j++) {
				System.out.println(i + " x " + j + " = " + (i * j));
			}
		}

		// 공포의 별 찍기
		for (int i = 0; i < 5; i++) {
			String star = "";
			for (int j = 0; j < 5; j++) {
				star = star + "*";
			}
			System.out.println(star);
		}

		for (int i = 1; i <= 5; i++) {
			String star = "";
			for (int j = 0; i > j; j++) {
				star = star + "*";
			}
			System.out.println(star);
		}

		for (int i = 5; i >= 0; i--) {
			String star = "";
			for (int j = 0; i > j; j++) {
				star = star + "*";
			}
			System.out.println(star);
		}

		for (int i = 5; i > 0; i--) {
			String star = "";
			for (int j = 1; j <= 5; j++) {
				if (i > j) {
					star = star + " ";
				} else {
					star = star + "*";
				}
			}
			System.out.println(star);
		}
	}
}