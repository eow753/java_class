package com.yedam.list;

import java.util.ArrayList;
import java.util.List;

public class ListExample {
	public static void main(String[] args) {
		// List
		List<String> list = new ArrayList<String>();

		// list 크기, length == 크랙 변수
		int size = list.size();
		System.out.println("총 객체 수 : " + size);
		System.out.println();

		// list 객체 가져오기
		String skill = list.get(2);
		System.out.println("Index 2 : " + skill);
		System.out.println();

		// list 크기만큼 반복문 돌리는 방법
		for (int i = 0; i < list.size(); i++) {
			String str = list.get(i);
			System.out.println(i + " : " + str);
		}

		System.out.println();
		
		list.remove(2);
		
		for(int i = 0; i < list.size(); i++) {
			String str = list.get(1);
			System.out.println(i+" : "+ );
		}
	}
}
