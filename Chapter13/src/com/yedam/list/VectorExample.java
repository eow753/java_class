package com.yedam.list;

import java.util.List;
import java.util.Vector;

public class VectorExample {
	public static void main(String[] args) {
		List<Board> list = new Vector<>();
		list.add(new Board("제목1", "내용1", "글쓴이1"));
		list.add(new Board("제목2", "내용2", "글쓴이2"));
		list.add(new Board("제목3", "내용3", "글쓴이3"));
		list.add(new Board("제목4", "내용4", "글쓴이4"));
		list.add(new Board("제목5", "내용5", "글쓴이5"));

		for (int i = 0; i < list.size(); i++) {
			// 첫번째방법 = 변수에 객체를 담아서 사용
			Board board = list.get(i);
			System.out.println(board.subject + "\t" + board.contents + "\t" + board.writer);

			// 두번째방법 = 변수에 담지 않고 list.get을 활용해서 사용
			System.out.println(list.get(i).subject + "\t" + list.get(i).contents + "\t" + list.get(i).writer);
		}

		list.remove(2);
		System.out.println();
		for (int i = 0; i < list.size(); i++) {
			// 첫번째방법 = 변수에 객체를 담아서 사용
//			Board board = list.get(i);
//			System.out.println(board.subject + "\t" + board.contents + "\t" + board.writer);

			// 두번째방법 = 변수에 담지 않고 list.get을 활용해서 사용
			System.out.println(list.get(i).subject + "\t" + list.get(i).contents + "\t" + list.get(i).writer);
		}
	}
}
