package com.yedam.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetExample {
	public static void main(String[] args) {
		Set<String> set = new HashSet<>();

		set.add("Java");
		set.add("JDBC");
		set.add("Servlet/JSP");
		set.add("Java");
		set.add("iBATIS");

		int size = set.size();
		System.out.println("총 개체 수 : " + size);

		Iterator<String> iterator = set.iterator();

		while (iterator.hasNext()) {
			String element = iterator.next();
			System.out.println("\t" + element);
		}

		set.remove("Java");
		set.remove("JDBC");

		// 향상된 for문
		for (String element : set) {
			System.out.println("\t" + element);
		}

		set.clear();
		System.out.println("총 객체 수 : " + set.size());
		if(set.isEmpty()) {
			System.out.println("객체가 존재하지 않습니다.");
		}
	}
}
