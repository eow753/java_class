package date221129;

public class Question2 {
	public static void main(String[] args) {
//	4) 아래와 같이 출력결과가 나오도록 실행코드를 구현한다.
//	- 출력결과
//	NOTEBOOK_MODE
//	한글2020을 통해 문서를 작성.
//	영화를 시청.
//	TABLET_MODE
//	안드로이드앱을 실행.
//	크롬을 통해 인터넷을 검색.
		PortableNotebook PNbook = new PortableNotebook("한글2020", "크롬", "영화", "안드로이드앱");
		Notebook nb = PNbook;
		Tablet tablet = PNbook;
		
		PNbook.writeDocumentaion();
		PNbook.watchVideo();
		PNbook.useApp();
		PNbook.searchInternet();
	}
}
