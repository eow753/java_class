package date221129;

import java.util.Scanner;

public class Question3 {
	public static void method1(Keypad keypad) {

	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean state = true;
		int gameType;

		int selectGame = (int) (Math.random() * 2) + 1;

		Keypad keypad = new RPGgame();
		
		if (selectGame == 1) {
			keypad = new RPGgame();
		} else if (selectGame == 2) {
			keypad = new ArcadeGame();
		}
		
		while (state) {
			System.out.println(
					"<< 1.LeftUP | 2.LeftDown | 3.RightUp | 4.RightDown | 5.ModeChange | 0.GameChange | 9.EXIT >>");
			int choice = Integer.parseInt(sc.nextLine());

			switch (choice) {
			case 0:
				if (selectGame == 1) {
					selectGame = 2;
					keypad = new ArcadeGame();
					break;
				} else if (selectGame == 2) {
					selectGame = 1;
					keypad = new RPGgame();
					break;
				}
				break;
			case 1:
				keypad.leftUpButton();
				break;
			case 2:
				keypad.leftDownButton();
				break;
			case 3:
				keypad.rightUpButton();
				break;
			case 4:
				keypad.rightDownButton();
				break;
			case 5:
				keypad.changeMode();
				break;
			case 9:
				state = false;
				System.out.println("EXIT");
				break;
			}
		}
	}
}
