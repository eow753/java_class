package date221129;

public class Question1 {
	public static void main(String[] args) {
		Keypad rpgGame = new RPGgame();
		rpgGame.leftUpButton();
		rpgGame.rightUpButton();
		rpgGame.changeMode();
		rpgGame.rightUpButton();
		rpgGame.rightDownButton();
		rpgGame.leftDownButton();
		rpgGame.changeMode();
		rpgGame.rightDownButton();
		System.out.println("======================");
		Keypad arcadeGame = new ArcadeGame();
		arcadeGame.leftUpButton();
		arcadeGame.rightUpButton();
		arcadeGame.leftDownButton();
		arcadeGame.changeMode();
		arcadeGame.rightUpButton();
		arcadeGame.leftUpButton();
		arcadeGame.rightDownButton();		
	}
}
