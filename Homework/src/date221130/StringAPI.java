package date221130;

import java.util.Scanner;

public class StringAPI {
	public static void main(String[] args) {
		String str = "123456789";
		for (int i = str.length() - 1; i >= 0; i--) {
			System.out.println(str.charAt(i));
		}

		String firstWord = "abc";
		String secondWord = "abcd";
		String thirdWord = "abcde";
		String shortWord = firstWord + " : " + firstWord.length();

		if (firstWord.length() > secondWord.length()) {
			shortWord = secondWord + " : " + secondWord.length();
			if (secondWord.length() > thirdWord.length()) {
				shortWord = thirdWord + " : " + thirdWord.length();
			}
		} else {
			if (firstWord.length() > thirdWord.length()) {
				shortWord = thirdWord + " : " + thirdWord.length();
			}
		}

		System.out.println(shortWord);

		String id = "abcd";
		String pw = "123456789";

		if (id.length() > 3 && pw.length() >= 8) {
			System.out.println("결과 : 사용 가능");
		} else {
			System.out.println("결과 : 사용 불가능");
		}

		Scanner sc = new Scanner(System.in);

		System.out.println("생년월일 > ");

		String birth = sc.nextLine();
		int birthNo = Integer.parseInt(birth.substring(0, 2));

		if (birthNo <= 22) {
			System.out.println("나이 : " + ((22 - birthNo) + 1));
		} else if (birthNo > 22) {
			System.out.println("나이 : " + (123 - birthNo));
		}
		
		
	}
}
