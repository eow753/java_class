package date221130;

import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) {
		// 1.문자열 뒤집기
		String str = "123456789";
		for (int i = (str.length() - 1); i >= 0; i--) {
			System.out.print(str.charAt(i));
		}
		System.out.println();

		// 2. 세 개의 임의 단어 3개 중 가장 짧은 단어와 길이 출력하기
		String word1 = "대한민국";
		String word2 = "코리아";
		String word3 = "한국";
		if (word1.length() < word2.length() && word1.length() < word3.length()) {
			System.out.println(word1 + ", 길이 : " + word1.length());
		} else if (word1.length() > word2.length() && word2.length() < word3.length()) {
			System.out.println(word2 + ", 길이 : " + word2.length());
		} else if (word1.length() > word3.length() && word2.length() > word3.length()) {
			System.out.println(word3 + ", 길이 : " + word3.length());
		}

		// 3. ID와 비밀번호를 입력 받아 ID가 3글자보다 크고 비밀번호가 8글자 이상
		// 사용할 수 있는 ID와 PW입니다. 출력하기
		Scanner sc = new Scanner(System.in);
		System.out.println("ID를 입력하세요. >");
		String id = sc.nextLine();
		System.out.println("비밀번호를 입력하세요. >");
		String pw = sc.nextLine();
		if (id.length() > 3 && pw.length() >= 8) {
			System.out.println("사용할 수 있는 ID와 PW입니다.");
		}

		// 4. 생년월일 입력 후 나이 출력하기(220101 -> 1, 1922)
		// 예시) 950101 -> 28 / 001013 -> 23
		System.out.println("생년월일을 입력하세요. >");
		String birth = sc.nextLine();
		String year = birth.substring(0, 2);

		Integer obj1 = new Integer(year);
		int value1 = obj1.intValue();
		if (22 < value1 && value1 < 100) {
			value1 = value1 + 1900;
		} else {
			value1 = value1 + 2000;
		}

		System.out.println(2022 - value1 + 1);
	}
}
