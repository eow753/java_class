package date221130;

import java.util.Scanner;

public class Exam02 {
	public static void main(String[] args) {
//	#모든 문자열은 소문자로 받아서 진행 한다.
//
//	// 1) 문자열 개수 세기
//	-> 입력 문자열에서 알파벳, 숫자, 공백의 개수를 구하시오.
//	예시) 
//	-> 입력 : 1a2b3c4d 5e
//	-> 출력 : 문자 :5개, 숫자:5개, 공백 : 1개
		Scanner sc = new Scanner(System.in);

		System.out.println("문자열을 입력해 주세요. >");
		String keyword1 = sc.nextLine();
		String keyword2 = keyword1;
		int count1 = keyword1.length();

		for (int i = 0; i < 10; i++) {
			keyword2 = keyword2.replace(String.valueOf(i), "");
		}
		int count2 = count1 - keyword2.length();

		int count3 = keyword1.length() - keyword1.replace(" ", "").length();

		System.out.println("문자 : " + (count1 - count2 - count3) + "개, 숫자 : " + count2 + "개, 공백 : " + count3 + "개");

//
//	//2) 중복이 안되는 문자열에서 두 문자사이의 거리 구하기
//	조건 : 입력되는 두 문자를 제외한 가운데 문자의 갯수를 두 문자간 거리로 한다.
//	예시)
//	-> 입력 : "abcdefghijklmnopqrstuvwxyz"
//	-----------------------------------
//	-> 입력 : 첫번째 문자 : c
//	-> 입력 : 두번째 문자 : f
//	-> 출력 : 두 문자간의 거리 : 2
//	------------------------------------
//	-> 입력 : 첫번째 문자 : e
//	-> 입력 : 두번째 문자 : a
//	-> 출력 : 두 문자간의 거리 : 3

		System.out.println("문자열을 입력해 주세요. >");
		String keyword3 = sc.nextLine();

		System.out.println("첫 번째 문자를 입력해 주세요. >");
		String key1 = sc.nextLine();
		System.out.println("두 번째 문자를 입력해 주세요. >");
		String key2 = sc.nextLine();

		int result = 0;

		if (keyword3.indexOf(key1) > keyword3.indexOf(key2)) {
			result = keyword3.indexOf(key1) - keyword3.indexOf(key2) - 1;
		} else if (keyword3.indexOf(key1) < keyword3.indexOf(key2)) {
			result = keyword3.indexOf(key2) - keyword3.indexOf(key1) - 1;
		}
		System.out.println("두 문자 간의 거리 : " + result);

//
//	//3) 중복문자 제거
//	입력 : aaabbccceedddd
//	출력 : abced

		System.out.println("문자열을 입력해 주세요. >");
		String keyword4 = sc.nextLine();

		String keyword5 = "";

		for (int i = 0; i < keyword4.length(); i++) {
			if (keyword4.indexOf(keyword4.charAt(i)) == i)
				keyword5 += keyword4.charAt(i);
		}
		
		System.out.println(keyword5);
	}
}
