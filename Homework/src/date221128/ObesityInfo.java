package date221128;

public class ObesityInfo extends StandardWeightInfo {
//3) ObesityInfo 클래스를 정의한다.
//- StandardWeightInfo 클래스를 상속한다.
//- 메소드는 다음과 같이 정의한다.
//(1) public void getInformation() : 이름, 키, 몸무게와 비만도를 출력하는 기능
//(2) public double getObesity() : 비만도를 구하는 기능
//( * 비만도 : (Weight - 표준 체중)/표준체중 * 100 )

	// 필드

	// 생성자
	public ObesityInfo(String name, int height, int weight) {
		super(name, height, weight);
	}

	// 메소드
	@Override
	public void getInformation() {
		System.out.println(name + "님의 신장" + height + ", 몸무게 " + weight + ", " + getObesity() + "입니다.");
	}

	public String getObesity() {
		double standardWeight = super.getStandardWeight();
		double obesity = (weight - standardWeight) / standardWeight * 100;
		String result;
		if (obesity < 14.93) {
			result = "저체중";
		} else if (obesity >= 14.93 && obesity < 22.57) {
			result = "정상";
		} else if (obesity >= 22.57 && obesity < 25.50) {
			result = "과체중";
		} else {
			result = "비만";
		}
		return result;
	}
}
