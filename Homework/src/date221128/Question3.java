package date221128;

import java.util.Scanner;

public class Question3 {

	public static void main(String[] args) {
// 4) 아래와 같은 출력결과가 나오도록 실행코드를 구현한다.
// - 출력결과
// 영화제목 : 추격자
// 감독 : 7
// 배우 : 5
// 영화총점 : 12
// 영화평점 : ☆☆☆☆
// =====================
// 공연제목:지킬앤하이드
// 감독:9
// 배우:10
// 공연총점 : 46
// 공연평점 : ☆☆☆☆☆
// - 조건
// 관객수, 총점과 평점은 입력되는 값에 따라 변동될 수 있습니다.

		Scanner sc = new Scanner(System.in);
		boolean state = true;
		
		Movie movie = new Movie("추격자", 7, 5, "영화");
		Performance performance = new Performance("지킬앤하이드", 9, 10, "뮤지컬");
		movie.audience = 0;
		movie.point = 0;
		performance.audience = 0;
		performance.point = 0;

		while (state) {	
			System.out.println("1. 추격자 || 2. 지킬앤하이드 || 3. 종료");
			System.out.println("작품을 선택해 주세요. >");	
			int choice = Integer.parseInt(sc.nextLine());			
			
			if(choice == 1) {
				System.out.println("평점을 입력해 주세요. >");
				int score = Integer.parseInt(sc.nextLine());
				movie.setTotalScore(score);
			} else if(choice == 2) {
				System.out.println("평점을 입력해 주세요. >");
				int score = Integer.parseInt(sc.nextLine());
				performance.setTotalScore(score);								
			} else if(choice == 3) {
				state = false;
			}

			movie.getInformation();
			System.out.println("=====================");
			performance.getInformation();
		}
	}

}
