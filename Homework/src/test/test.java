package test;

import java.util.Scanner;

public class test {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int diceNum = 0;
		int[] diceArr = null;
		boolean findFive = true;
		int rollNum = 0;

		while (true) {
			System.out.println("===1.주사위 크기 2.주사위 굴리기 3.결과 보기 4.가장 많이 나온 수 5.종료 ===");
			System.out.println("메뉴 > ");

			int choice = Integer.parseInt(sc.nextLine());

			// 1. 주사위 크기
			if (choice == 1) {
				System.out.println("주사위 크기 > ");
				diceNum = Integer.parseInt(sc.nextLine());

				if (diceNum < 5 && 10 < diceNum) {
					System.out.println("주사위 크기 범위를 벗어납니다. 5 ~ 10 중에 하나를 입력해 주세요.");
				}

				// 2. 주사위 굴리기
			} else if (choice == 2) {
				int i = 1;
				diceArr = new int[diceNum];

				while (findFive) {
					int dice = (int) (Math.random() * diceNum) + 1;

					i++;
					rollNum = i;

					for (int j = 0; j <= diceNum - 1; j++) {
						diceArr[j] += 1;
					}

					if (dice == 5) {
						findFive = false;
					}
				}

				System.out.println("5가 나올 때까지 주사위를 " + rollNum + "번 굴렸습니다.");

				// 3. 결과 보기
			} else if (choice == 3) {
				for (int i = 0; i < diceArr.length; i++) {
					System.out.println((i + 1) + "은 " + diceArr[i] + "번 나왔습니다.");
				}

				// 4. 가장 많이 나온 수
			} else if (choice == 4) {
				int max = diceArr[0];
				for (int i = 0; i < diceArr.length; i++) {
					if (max < diceArr[i]) {
						max = diceArr[i];
					}
				}
				System.out.println("가장 많이 나온 수는 " + max + "입니다.");

				// 5. 종료
			} else if (choice == 5) {
				System.out.println("프로그램 종료");
				break;
			}
		}
	}
}
