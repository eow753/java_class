package test;

import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] dice = null;
		int size = 0;

		boolean run = true;

		while (run) {
			System.out.println("===1.주사위 크기 2.주사위 굴리기 3.결과 보기 4.가장 많이 나온 수 5.종료 ===");
			System.out.println("메뉴 > ");

			String selectNo = sc.nextLine();

			switch (selectNo) {
			case "1":
				System.out.println("주사위 크기 > ");
				size = Integer.parseInt(sc.nextLine());

				if (size < 5 || size > 10) {
					System.out.println("주사위 크기 범위를 벗어납니다. 5 ~ 10 중에 하나를 입력해 주세요.");
				}

				break;

			case "2":
				dice = new int[size];
				int count = 0;
				while (true) {
					int random = (int) (Math.random() * size) + 1;
					dice[random - 1] = dice[random - 1] + 1;
					count++;
					if (random == 5) {
						break;
					}
				}
				System.out.println("5가 나올 때 까지 주사위를 " + count + "번 굴렸습니다.");
				break;
			case "3":
				for (int i = 0; i < dice.length; i++) {
					System.out.println((i + 1) + "은 " + dice[i] + "번 나왔습니다.");
				}

				break;
			case "4":
				int max = 0;
				for (int num : dice) {
					if (max < num) {
						max = num;
					}
				}
				System.out.println("가장 많이 나온 수는 " + max + "입니다.");
				break;
			case "5":
				System.out.println("프로그램 종료");
				run = false;
				break;
			}
		}
	}
}
