package test;

public class Exam02 {
	public static void main(String[] args) {
		Student std1 = new Student();
		std1.setStdName("김또치");
		std1.setStdType("컴퓨터 공학과");
		std1.setStdClass("2학년");
		std1.setPrograming(50);
		std1.setDataBase(60);
		std1.setOs(90);
		System.out.println("이름 : " + std1.getStdName());
		System.out.println("학과 : " + std1.getStdType());
		System.out.println("학년 : " + std1.getStdClass());
		System.out.println("프로그래밍 언어 점수 : " + std1.getPrograming());
		System.out.println("데이터베이스 점수 : " + std1.getDataBase());
		System.out.println("운영체제 점수 : " + std1.getOs());
	}
}
