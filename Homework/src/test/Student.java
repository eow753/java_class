package test;

public class Student {
	// 필드
	private String stdName;
	private String stdType;
	private String stdClass;
	private int programing;
	private int dataBase;
	private int os;

	// 생성자
	// 클래스를 통한 객체를 생성할 때 첫 번재로 수행하는 일들을 모아두는 곳.
	// 필드에 대한 데이터를 객체를 생성할 때 초기화 할 예정이라면.
	// 생성자에서 this 키워드를 활용해서 필드 초기화 하면 됨.
	
	// 메소드

	public String getStdName() {
		return stdName;
	}

	public void setStdName(String stdName) {
		this.stdName = stdName;
	}

	public String getStdType() {
		return stdType;
	}

	public void setStdType(String stdType) {
		this.stdType = stdType;
	}

	public String getStdClass() {
		return stdClass;
	}

	public void setStdClass(String stdClass) {
		this.stdClass = stdClass;
	}

	public int getPrograming() {
		return programing;
	}

	public void setPrograming(int programing) {
		this.programing = programing;
	}

	public int getDataBase() {
		return dataBase;
	}

	public void setDataBase(int dataBase) {
		this.dataBase = dataBase;
	}

	public int getOs() {
		return os;
	}

	public void setOs(int os) {
		this.os = os;
	}
}
