package com.yedam.homwork;

import java.util.Scanner;

public class date221124 {
	public static void main(String[] args) {
		// 문제2) 다음은 키보드로부터 상품 수와 상품 가격을 입력받아서
		// 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총 합을 구하는 프로그램을 작성하세요.
		// 1) 메뉴는 다음과 같이 구성하세요.
		// 1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료
		// 2) 입력한 상품 수만큼 상품명과 해당 가격을 입력받을 수 있도록 구현하세요.
		// 3) 제품별 가격을 출력하세요.
		// 출력예시, "상품명 : 가격"
		// 4) 분석기능은 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총합을 구합니다.
		// 5) 종료 시에는 프로그램을 종료한다고 메세지를 출력하도록 구현하세요.

		Scanner sc = new Scanner(System.in);
		int goodsNum = 0;
		Goods[] goodsArr = null;

		while (true) {
			System.out.println("1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료");
			System.out.println("선택지를 입력해 주세요. > ");
			int selectNo = Integer.parseInt(sc.nextLine());

			if (selectNo == 1) {
				System.out.println("1. 상품 수 ");
				System.out.println("상품 수를 입력해 주세요. > ");
				goodsNum = Integer.parseInt(sc.nextLine());

			} else if (selectNo == 2) {
				goodsArr = new Goods[goodsNum];
				System.out.println("2. 상품 정보 및 가격입력");

				for (int i = 0; i < goodsArr.length; i++) {
					Goods good = new Goods();
					System.out.println("상품 이름을 입력해 주세요.");
					good.goodsName = sc.nextLine();
					System.out.println("상품의 가격을 입력해 주세요.");
					good.goodsPrice = Integer.parseInt(sc.nextLine());

					goodsArr[i] = good;
				}

			} else if (selectNo == 3) {
				for (int i = 0; i < goodsArr.length; i++) {
					System.out.println(goodsArr[i].goodsName + " : " + goodsArr[i].goodsPrice);
				}

			} else if (selectNo == 4) {
				int max = 0;
				String maxName = "";
				int maxNum = 0;
				int sum = 0;

				for (int i = 0; i < goodsArr.length; i++) {
					if (max < goodsArr[i].goodsPrice) {
						max = goodsArr[i].goodsPrice;
						maxName = goodsArr[i].goodsName;
						maxNum = i;
					}
				}
				for (int i = 0; i < goodsArr.length; i++) {
					if (maxNum != i) {
						sum += goodsArr[i].goodsPrice;
					}
				}
				System.out.println(maxName);
				System.out.println(sum);

			} else if (selectNo == 5) {
				System.out.println("프로그램 종료");
				break;
			}
		}
	}
}