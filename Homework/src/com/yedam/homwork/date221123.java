package com.yedam.homwork;

import java.util.Scanner;

public class date221123 {
	public static void main(String[] args) {
//		// 주어진 배열을 이용하여 다음 내용을 구현하세요.
//		int[] arr1 = { 10, 20, 30, 50, 3, 60, -3 };
//
//		// 문제1. 주어진 배열 중에서 값이 60인 곳의 인덱스를 출력해보자.
//		System.out.println("문제1");
//		for (int i = 0; i < arr1.length; i++) {
//			if (arr1[i] == 60) {
//				System.out.println(i);
//			}
//		}
//
//		// 문제2. 주어진 배열의 인덱스가 3인 곳은 출력하지 말고, 나머지만 출력해보자.
//		System.out.println("문제2");
//		for (int i = 0; i < arr1.length; i++) {
//			if (i != 3) {
//				System.out.println(arr1[i]);
//			}
//		}
//
//		// 문제3. 주어진 배열 안의 변경하고 싶은 값의 인덱스 번호를 입력받아, 그 값을 1000으로 변경해보자.
//		// 입력) 인덱스: 3 -> {10, 20, 30, 1000, 3, 60, -3}
//		System.out.println("문제3");
//		System.out.println("변경하고 싶은 값의 인덱스 번호를 입력해 주세요. >");
//		Scanner sc = new Scanner(System.in);
//		int choice = sc.nextInt();
//		for (int i = 0; i < arr1.length; i++) {
//			if (choice == i) {
//				arr1[i] = 1000;
//			}
//		}
//
//		// 확인용
//		for (int i = 0; i < arr1.length; i++) {
//			System.out.println(arr1[i]);
//		}
//
//		// 문제4. 주어진 배열의 요소에서 최대값과 최소값을 구해보자.
//		System.out.println("문제4");
//		int max = arr1[0];
//		int min = arr1[0];
//		for (int i = 0; i < arr1.length; i++) {
//			if (max < arr1[i]) {
//				max = arr1[i];
//			}
//			if (min > arr1[i]) {
//				min = arr1[i];
//			}
//		}
//		System.out.println("최대값은 " + max + ", 최소값은" + min + "입니다.");
//
//		// 문제5. 별도의 배열을 선언하여 양의 정수 10개를 입력받아 배열에 저장하고, 배열에 있는 정수 중에서 3의 배수만 출력해보자.
//		System.out.println("문제5");
//		int[] intArr = new int[10];
//		for (int i = 0; i < intArr.length; i++) {
//			System.out.println((i + 1) + "번째 양의 정수를 입력해 주세요. >");
//			intArr[i] = sc.nextInt();
//		}
//		for (int i = 0; i < intArr.length; i++) {
//			if (intArr[i] % 3 == 0) {
//				System.out.println(intArr[i]);
//			}
//		}

		// 추가문제 Lotto 생성
		// 1) 1~45 사이의 수를 랜덤으로 추출합니다.
		// 2) 랜덤으로 추출한 번호는 중복이 되선 안됩니다.
		// 3) 번호는 6개만 추출합니다.
		System.out.println("추가문제");					
		int[] lotto = new int[6];
		for (int i = 0; i < lotto.length; i++) {
			int lukNum = (int) (Math.random() * 45) + 1;
			for (int j = 0; j <= i; j++) {
				if (lukNum != lotto[j]) {
					lotto[i] = lukNum;
				}
			}
		}
		
		for (int i = 0; i < lotto.length; i++) {
			System.out.println(lotto[i]);
		}
	}
}
