package com.yedam.homwork;

import java.util.Scanner;

public class day221125 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Product[] pd = null;
		int pdCount = 0;

		while (true) {
			System.out.println("1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료");
			System.out.println("입력 > ");

			String selectNo = sc.nextLine();

			// 1. 상품 수
			if (selectNo.equals("1")) {
				System.out.println("상품 수 > ");
				pdCount = Integer.parseInt(sc.nextLine());

				// 2. 상품 및 가격 입력
			} else if (selectNo.equals("2")) {
				pd = new Product[pdCount];

				for (int i = 0; i < pd.length; i++) {
					// 상품 객체 생성
					Product product = new Product();
					System.out.println((i + 1) + "번째 상품");

					System.out.println("상품 명 > ");
					String name = sc.nextLine();
					product.ProductName = name;

					System.out.println("상품 가격 > ");
					product.price = Integer.parseInt(sc.nextLine());

					pd[i] = product;
					System.out.println("========");
				}

				// 3. 제품별 가격
			} else if (selectNo.equals("3")) {
				for (int i = 0; i < pd.length; i++) {
					String name = pd[i].ProductName;
					int price = pd[i].price;

					System.out.println("상품 명 : " + name);
					System.out.println("상품 가격 : " + pd[i].price);
				}

				// 4. 분석
			} else if (selectNo.equals("4")) {
				int max = pd[0].price;
				int sum = 0;
				for (int i = 0; i < pd.length; i++) {
					if (max < pd[i].price) {
						max = pd[i].price;
					}
					sum += pd[i].price;
				}
				System.out.println("제일 비싼 상품 가격 : " + max);
				System.out.println("제일 비싼 상품을 제외한 상품 총 합 : " + (sum - max));

				// 5. 종료
			} else if (selectNo.equals("5")) {
				System.out.println("프로그램이 종료되었습니다.");
				break;
			}
		}
	}
}
