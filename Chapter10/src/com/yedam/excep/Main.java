package com.yedam.excep;

public class Main {
	public static void main(String[] args) {
//		String data = null;
//		System.out.println(data.toString());

		int[] intAry = new int[3];
		intAry[0] = 1;
		intAry[1] = 2;
		intAry[3] = 3;
//		intAry[10] = 10;
//		selectGame = 2;

		Example exam = new Exam();
		Exam exam2 = (Exam) exam;
		
		//예외케이스->instanceof
		//Example 자신의 객체를 만듬
		Example exam3 = new Example();
		// 자신의 객체를 자식으로 바꾸는 과정
		Exam exam4 = (Exam)exam3;
		// 예외처리
		// exam3 과 exam이 같은 객체인지 확인하고 나서 강제 타입 변환
		if(exam3 instanceof Exam) {
			Exam exam5 = (Exam)exam3;
		}
	}
}
