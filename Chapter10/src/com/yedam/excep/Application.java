package com.yedam.excep;

public class Application {
	public static void main(String[] args) {
//		try {
//			//예외가 발생할만한 코드
//		}catch () {
//			//예외가 발생 후 처리하는 코드
//		}
		// 1) 자바가 먼저 컴파일 후 인식해서 예외처리
		try {
			String str = "자바";
			Integer.parseInt(str);
			System.out.println("변환 완료!!");
			
			Class clazz = Class.forName("java.lang.String2");
			
			ㅐㅏSystem.out.println("예외처리 발생!!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace(); // 예외가 발생한 경로를 추적하고 console 출력하세요.
			System.out.println("catch문으로 이동");
		}
	}
}
