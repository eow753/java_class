package com.yedam.test;

public class SimplePayment implements Payment{
	// 필드
		public double cardRatio;
		
		// 생성자
		public SimplePayment(double cardRatio) {
			this.cardRatio = cardRatio;
		}
		
		// 메소드
		@Override
		public int online(int price) {
			double online = ONLINE_PAYMENT_RATIO * 0.01;
			double result = price - (price * (cardRatio + online));
			return (int)result;
		}

		@Override
		public int offline(int price) {
			double offline = OFFLINE_PAYMENT_RATIO * 0.01;
			double result = price - (price * (cardRatio + offline));
			return (int)result;
		}

		@Override
		public void showInfo() {
			System.out.println("*** 카드로 결제 시 할인정보");
			double online = ONLINE_PAYMENT_RATIO * 0.01;
			double offline = OFFLINE_PAYMENT_RATIO * 0.01;
			System.out.println("온라인 결제시 총 할인율 : " + (cardRatio + online));
			System.out.println("오프라인 결제시 총 할인율 : " + (cardRatio + offline));
		}
}
