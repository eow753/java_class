package com.yedam.test;

public class Quest4 {
	public static void main(String[] args) {
		String[] arr = { "010102-4", "991012-1", "960304-1", "881012-2", "040403-3" };

		int male = 0;
		int female = 0;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].charAt(7) == '1' || arr[i].charAt(7) == '3') {
				male += 1;
			} else if (arr[i].charAt(7) == '2' || arr[i].charAt(7) == '4') {
				female += 1;
			}
		}

		System.out.println("남 " + male + " 여 " + female);
	}
}
