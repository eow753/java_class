package com.yedam.test;

public interface Payment {
	// 필드
	public int ONLINE_PAYMENT_RATIO = 5;
	public int OFFLINE_PAYMENT_RATIO = 3;
	
	// 메소드
	public int online(int price);
	public int offline(int price);
	public void showInfo();
}
