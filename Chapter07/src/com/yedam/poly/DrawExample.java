package com.yedam.poly;

public class DrawExample {
	public static void main(String[] args) {
		Draw circle = new Circle();
		circle.x = 1;
		circle.y = 2;
		circle.draw();
		
		circle = new Rectangle();
		circle.draw();
	}
}
