package com.yedam.poly;

public class Parent extends GrandParent{
	public String field;
	
	public void method1() {
		System.out.println("parent-methode1");
	}

	public void method2() {
		System.out.println("parent-methode2");
	}
}
