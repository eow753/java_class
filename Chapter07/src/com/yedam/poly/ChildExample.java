package com.yedam.poly;

public class ChildExample {

	public static void main(String[] args) {
		Child child = new Child();
		// 클래스간의 자동타입변환
		// 부모 클래스에 있는 메소드를 사용하되 
		// 단, 자식 클래스에 재정의가 되어 있으면
		// 자식 클래스에 재정의된 메소드를 사용하겠습니다.
		Parent parent = child;
		parent.method1();
		parent.method2();
//		parent.method3();
	}

	public static void method1(Parent parent) {
		if(parent instanceof Child ) {
			Child child = (Child) parent;
			System.out.println("변환 성공");
		}else {
			System.out.println("변환 실패");
		}
	}
}