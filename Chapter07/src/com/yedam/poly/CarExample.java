package com.yedam.poly;

public class CarExample {
	public static void main(String[] args) {
		Car car = new Car();
		for (int i = 0; i <= 5; i++) {
			int problemLoc = car.run();

			switch (problemLoc) {
			case 1:
				System.out.println("앞 왼쪽 HanKookTire 교환");
				car.frontLeftTire = new HanKookTire("앞왼쪽", 15);
				break;
			case 2:
				System.out.println("앞 오른쪽 KumhoTire 교환");
				car.frontRightTire = new HanKookTire("앞오른쪽", 15);
				break;
			case 3:
				System.out.println("뒤 왼쪽 HanKookTire 교환");
				car.backLeftTire = new HanKookTire("뒤왼쪽", 15);
				break;
			case 4:
				System.out.println("뒤 왼쪽 KumhoTire 교환");
				car.backRightTire = new HanKookTire("뒤오른쪽", 15);
				break;
			}
		}
	}
}
