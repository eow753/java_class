package com.yedam.poly;

public class Child extends Parent {
	
	@Override
	public void method4() {
		System.out.println("나는 손자");
	}

	public String field2;

	@Override
	public void method2() {
		System.out.println("child-methode2");
	}
	
	public void method3() {
		System.out.println("parent-methode3");
	}
	
}
