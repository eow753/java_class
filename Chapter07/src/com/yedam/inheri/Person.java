package com.yedam.inheri;

public class Person extends People {
	public int age;
	// 자식 객체를 만들때, 생성자를 통해 만든다.
	public Person(String name, String ssn) {
		super(name, ssn);
	}
}
