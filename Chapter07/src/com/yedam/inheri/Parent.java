package com.yedam.inheri;

public class Parent {
	public String firstName = "kim";
	public String lastName;
	public String DNA = "A";
	// 2) 상속 대상에서 제외
	public char bloodType = 'O';
	public int age;

//	private char bloodType = 'B';

	protected void method1() {
		System.out.println("parent class -> method1 Call");
	}

	public void method2() {
		System.out.println("parent class -> method2 Call");
	}
}
