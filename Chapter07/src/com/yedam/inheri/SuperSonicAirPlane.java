package com.yedam.inheri;

public class SuperSonicAirPlane extends AirPlane {
	// 필드
	public static final int NORMAR = 1;
	public static final int SUPERSONIC = 2;
	public int flyMode = NORMAR;
	@Override
	public void fly() {
		if(flyMode == SUPERSONIC) {
			System.out.println("초음속비행합니다.");
		} else {			
			super.fly();
		}
	}

	// 생성자

	// 메소드
	
}
