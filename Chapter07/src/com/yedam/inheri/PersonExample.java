package com.yedam.inheri;

public class PersonExample {
	public static void main(String[] args) {
		Person p1 = new Person("고희동", "123456-9876541");
		System.out.println("name : " + p1.name);
		System.out.println("ssn : " + p1.ssn);
		//상속대상에서제외
		System.out.println("age : " + p1.age);
	}
}
