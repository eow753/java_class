package com.yedam.inheri;

public class DmbPhoneExample {
	public static void main(String[] args) {
		DmbCellPhone dmb = new DmbCellPhone("자바폰", "검정", 10);
		System.out.println(dmb.model);
		System.out.println(dmb.color);
		System.out.println(dmb.channel);
		dmb.powerOn();
		dmb.bell();
		dmb.hangUp();
		dmb.turnOnDmb();
		dmb.turnOffDmb();
		dmb.powerOff();
	}
}
