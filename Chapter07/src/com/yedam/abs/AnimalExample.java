package com.yedam.abs;

public class AnimalExample {

	public static void main(String[] args) {
		Cat cat = new Cat();
		cat.sound();
		
		System.out.println();

		Animal animal = new Cat();
		animal.sound();
		animal();
	}

}
