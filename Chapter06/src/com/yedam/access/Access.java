package com.yedam.access;

public class Access {
	// public 		어디서든 누구나 다 접근 가능
	// protected 	상속 받은 상태에서 부모-자식간에 사용(패키지가 달라도 사용 가능)
	// 				패키지가 다르면 사용 못 함, 같은 패키지에서만 사용 가능
	// default	 	패키지가 다르면 사용 못 함, 같은 패키지에서만 사용 가능
	// private		내가 속한 클래스에서만 사용 가능
	
	// 필드
	public String free;
	protected String parent;
//	private String privacy;
	String basic;
	
	// 생성자
	
	// 메소드
	public void free() {
		System.out.println("접근이 가능합니다.");
		privacy()
	}
	private void privacy() {
		System.out.println("접근이 불가능합니다.");
	}
}
