package com.yedam.access;

public class Application {
	public static void main(String[] args) {
		Access access = new Access();

		// public
		access.free = "free";

		// protected
		access.parent = "parent";

		// private
//		access.privacy = "privacy";

		// default
		access.basic = "basic";
		
		access.free();
		
		Singleton obj1 = Singleton.getInstance();
		Singleton obj2 = Singleton.getInstance();
	}
}
