package com.yedam.oop;

public class Student {
	// 필드
	String name;
	String school;
	int number;

	int kor;
	int math;
	int eng;

	// 생성자
	public Student() {
	};

	public Student(String name, String school, int number, int kor, int math, int eng) {
		this.name = name;
		this.school = school;
		this.number = number;
		this.kor = kor;
		this.math = math;
		this.eng = eng;
	};

	// 메소드
	void getInfo() {
		System.out.println("학생의 이름 : " + name);
		System.out.println("학생의 학교 : " + school);
		System.out.println("학생의 학번 : " + number);
		System.out.println("총    점 : " + getSum());
		System.out.println("평    균 : " + getAvg() + "\n");
	}

	int getSum() {
		return kor + math + eng;
	}
//	int getSum(int... values) {
//		int sum = 0;
//		for (int i = 0; i < values.length; i++) {
//			sum += values[i];
//		}
//		return sum;
//	}

	double getAvg() {
		return getSum() / (double)3;
	}
//	double getAvg(int... values) {
//		return getSum(values) / (double)values.length;
//	}
}
