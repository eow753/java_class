package com.yedam.oop;

public class Application2 {
	public static void main(String[] args) {
//		Calculator cl = new Calculator();
//
//		int sumResult = cl.sum(1, 2);
//		double subResult = cl.sub(10, 20);
//		System.out.println(sumResult);
//		System.out.println(subResult);
//		System.out.println(cl.result("메소드 연습"));
//		cl.result("메소드 연습");
//
//		Computer myCom = new Computer();
//		int result = myCom.sum(1, 2, 3);
//		System.out.println(result);
//		result = myCom.sum(1, 2, 3, 4, 5, 6);
//		System.out.println(result);

		Book javaBook = new Book("혼자 공부하는 자바", "학습서", "24000원", "한빛미디어", "yedam-001");
		javaBook.getInfo();
		Book linuxBook = new Book("이것이 리눅스다", "학습서", "32000원", "한빛미디어", "yedam-002");
		linuxBook.getInfo();
		Book javascriptBook = new Book("자바스크립트 파워북", "학습서", "22000원", "어포스트", "yedam-003");
		javascriptBook.getInfo();

		
		Student stdGo = new Student("고길동", "예담고등학교", 221124, 80, 75, 70);
		stdGo.getInfo();

		Student stdKim1 = new Student();
		stdKim1.name = "김둘리";
		stdKim1.school = "예담고등학교";
		stdKim1.number = 221125;
		stdKim1.kor = 75;
		stdKim1.math = 75;
		stdKim1.eng = 75;
		stdKim1.getInfo();
		
		Student stdKim2 = new Student();
		stdKim2.name = "김또치";
		stdKim2.school = "예담고등학교";
		stdKim2.number = 221126;
		stdKim2.kor = 75;
		stdKim2.math = 75;
		stdKim2.eng = 75;
		stdKim2.getInfo();
	}
}
