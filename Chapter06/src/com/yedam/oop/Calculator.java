package com.yedam.oop;

public class Calculator {
	// 필드
	// 정적 필드
	static double pi = 3.14;

	// 생성자

	// 메소드
	static int plus(int x, int y) {
		return x + y;
	}

	int sum(int a, int b) {
		return a + b;
	}

	double sum(double a, double b) {
		return a + b;
	}

	int sum(int a) {
		return a;
	}

	double sum(double a, int b) {
		return a + b;
	}

	double sum(int b, double a) {
		return a + b;
	}

	double sub(int a, int b) {
		return a - b;
	}

	String result(String value) {
		String temp = "value return 테스트" + value;
		return temp;
	}
}
