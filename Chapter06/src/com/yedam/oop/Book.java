package com.yedam.oop;

public class Book {
	// 필드
	String name;
	String type;
	String price;
	String press;
	String isbn;
	
	// 생성자
	public Book(String name, String type, String price, String press, String isbn) {
		this.name = name;
		this.type = type;
		this.price = price;
		this.press = press;
		this.isbn = isbn;
	}
	
	// 메소드
	void getInfo() {
		System.out.println("책 이름 : " + name);
		System.out.println("# 내용");
		System.out.println("1) 종류 : " + type);
		System.out.println("2) 가격 : " + price);
		System.out.println("3) 출판사 : " + press);
		System.out.println("4) 도서번호 : " + isbn +"\n");
	}
}
