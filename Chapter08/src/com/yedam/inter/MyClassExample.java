package com.yedam.inter;

public class MyClassExample {

	public static void main(String[] args) {
		System.out.println("1)=========");
		MyClass myClass = new MyClass();
		myClass.rc.turnOn();
		myClass.rc.turnOff();
		System.out.println();
		System.out.println("2)=========");
		
		MyClass myClass2 = new MyClass (new Audio());
		
		MyClass myClass4 = new MyClass();
		myClass4.method(new Television());
	}

}
