package com.yedam.inter;

public class Rectangle implements getInfo {
	int width;
	int height;

	public Rectangle(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public void area() {
		System.out.println(width * height);
	}

	@Override
	public void round() {
		System.out.println((2 * width) + (2 * height));
	}
}
