package com.yedam.inter;

public interface Animal {
	void walk();
	void fly();
	void sing();
}
