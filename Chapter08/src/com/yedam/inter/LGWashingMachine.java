package com.yedam.inter;

public class LGWashingMachine implements WashingMachine {

	@Override
	public void dry() {
		System.out.println("건조 코스 진행");
	}

	@Override
	public void startBtn() {
		System.out.println("빨래 시작");
	}

	@Override
	public void pauseBtn() {
		System.out.println("빨래 일시 중지");
	}

	@Override
	public void stopBtn() {
		System.out.println("빨래 중지");
	}

	@Override
	public int changeSpeed(int speed) {
		int nowSpeed = 0;
		switch (speed) {
		case 1:
			nowSpeed = 1;
			break;

		default:
			break;
		}
		return 0;
	}

}
